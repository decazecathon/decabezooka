# decazooka008.py
# Please read decazooka001/.../007.py
# Using a third party decorator library
#
# This module is different from the examples in the previous programs
# Reason - Use of the third party "decorator" library
#

import functools
from decorator import decorator
import inspect


def InspectMe001 (f, *args, **kw):
    print ("Inside inspect me")
    return f(*args,**kw)


@decorator(InspectMe001)
def RandomFunc001(x,y):
    print ( x, y)

RandomFunc001("a", "b")

print ("---------------------------------------------------------------")

@decorator
def InspectMe002 (f, *args, **kw):
    print ("Inside inspect me")
    return f(*args,**kw)


@InspectMe002
def RandomFunc002(x,y):
    print ( x, y)

RandomFunc002("c", "d")

print ("At the end")


print ("At the end")


print ("At the end")

