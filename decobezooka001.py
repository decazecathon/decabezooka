# decazooka001.py

# What does functools do?
import functools
import inspect

#
# This is the basic template of a function decorator
def  DecorativeHat001 ( targetfunc: object):
    #
    # This prevents a side effect of the decoration
    @functools.wraps (targetfunc)
    # Hey, this is another new concept - a function inside another function (Nested function)
    def innerfunc():
        #
        print (":::1.1::: " + inspect.stack()[1][3]  + " ::: PRE ")
        targetfunc()
        print (":::1.1::: " + inspect.stack()[1][3]  + " ::: POST")
    return innerfunc

@DecorativeHat001
def SimpleFunc001():
    print ("Hey, I am just a simple func")

print (SimpleFunc001.__name__)
SimpleFunc001()
print (SimpleFunc001.__name__)

############################################################################################

#
# Almost identical to the above
# Note - Till now, both these functions being decorated don't return any values
#
def  DecorativeHat002 ( targetfunc: object):
    #
    # This prevents a side effect of the decoration
    @functools.wraps (targetfunc)
    def innerfunc():
        #
        print ("002 002 ")
        targetfunc()
    return innerfunc

@DecorativeHat002
def SimpleFunc002():
    print ("Hey, I am just a simple func 002")

#print (SimpleFunc002.__name__)
xfunc = SimpleFunc002
xfunc()


def SimpleFunc003():
    print ("Hey, I am just a simple func 003")

SimpleFunc003 = DecorativeHat001( SimpleFunc003 )
SimpleFunc003()




