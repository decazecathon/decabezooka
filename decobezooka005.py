# decazooka005.py
#
# Building on top of decazooka001/002/003/004.py
# Please go through these three programs first
# Note - When chaining decorators, construct the return statements intelligently
#
#  dl1-> dl2 -> dl3 -> dl4   =   dl1(dl2(dl3 (dl4 ( fn )))))
#

import functools

# The decorator to make it bold
def makebody(fn):
    # The new function the decorator returns
    @functools.wraps(fn)
    def wrapper():
        # Insertion of some code before and after
        return "<body>" + fn() + "</body>"
    return wrapper

# The decorator to make it italic
def makehtml(fn):
    # The new function the decorator returns
    @functools.wraps(fn)
    def wrapper():
        # Insertion of some code before and after
        return "<html>" + fn() + "</html>"
    return wrapper

@makehtml
@makebody
def say():
    return "hello"

print(say())
#outputs: <b><i>hello</i></b>

# This is the exact equivalent to
def say():
    return "hello"
say = makehtml(makebody(say))

print(say())
#outputs: <b><i>hello</i></b>

def subtract_50_from_num(fn):
    # The new function the decorator returns
    @functools.wraps(fn)
    def wrapper():
        # Insertion of some code before and after
        return fn() - 50
    return wrapper

# The decorator to make it italic
def add_100_to_num(fn):
    # The new function the decorator returns
    @functools.wraps(fn)
    def wrapper():
        # Insertion of some code before and after
        return 100 + fn()
    return wrapper

@add_100_to_num
@subtract_50_from_num
def numma():
    return 30

print(numma())


def add_footer_info(fn):
    # The new function the decorator returns
    @functools.wraps(fn)
    def wrapper():
        # Insertion of some code before and after
        return fn()  + "This is the end of the document. Footer"
    return wrapper

# The decorator to make it italic
def add_header_info(fn):
    # The new function the decorator returns
    @functools.wraps(fn)
    def wrapper():
        # Insertion of some code before and after
        return "This is the beginning of the document. Header " + fn()
    return wrapper

@add_header_info
@add_footer_info
def chumma():
    return "--<MIDDLE TEXT>--"

print(chumma())



