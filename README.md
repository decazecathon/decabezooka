# decabezooka

Python project that contains a number of programs to explain the use of Decorators in the Python programming language

The reader is advised to go through decobezooka???.py 

To make the project interesting, there is integration with gitlab-ci.

The pipeline is integrated with SonarCloud for static code analysis