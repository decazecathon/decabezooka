# decazooka002.py
#
# This is the basic template of a function decorator
# Here, the function 2b decorated returns a value

import functools
import inspect

def  DecorativeHat001 ( targetfunc: object):
    #
    # This prevents a side effect of the decoration
    @functools.wraps (targetfunc)
    def innerfunc():
        #
        print (":::1.1::: " + inspect.stack()[1][3]  + " ::: PRE ")
        x = targetfunc()
        print (":::1.1::: " + inspect.stack()[1][3]  + " ::: POST")
        return x
    return innerfunc

@DecorativeHat001
def SimpleFunc001():
    return {"name": "sanjiv", "age" : 51, "gender" : "M"}

retval = SimpleFunc001()
print (retval)


def SimpleFunc002():
    return {"name": "sanjiv", "age" : 51, "gender" : "M"}


"""
Lets summarize the basics of Python function decorators 

1. The underlying function i.e. the fn. being decorated is not modified in any way 
2. The decorator in a way allows the python module to perform actions before and after the fn. 
3. @Decorator 
   def MeNeedsDecoration
   
   equivalent to 
   MeNeedsDecoration  = Decorator (MeNeedsDecoration)
4. The decorator contains a new construct; a function within a function 
5. Basically, decazooka001.py and decazooka002.py are the same 

"""