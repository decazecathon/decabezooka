# decazooka004.py
#
# Building on top of decazooka001/002/003.py
# Please go through these three programs first
#

import functools

def DecorativeHat001(targetfunc: object):
    #
    # This prevents a side effect of the decoration
    @functools.wraps(targetfunc)
    def innerfunc():
        #
        print ("11111111111111111111111111111111111111111")
        targetfunc()
        #print(":::1.1::: " + inspect.currentframe().f_code.co_name + " ::: POST")
    return innerfunc



def DecorativeHat002(targetfunc: object):
    #
    # This prevents a side effect of the decoration
    @functools.wraps(targetfunc)
    def innerfunc():
        #
        print ("222222222222222222222222222222222222222222222")
        targetfunc()
    return innerfunc


@DecorativeHat001
@DecorativeHat002
def SimpleFunc001():
    print({"name": "ananya", "age": 17, "gender": "F"})

SimpleFunc001()

def SimpleFunc002():
    print({"name": "simplefunc002", "age": 17, "gender": "F"})

SimpleFunc002 = DecorativeHat001( DecorativeHat002( SimpleFunc002))
SimpleFunc002()

"""
1. Multiple decorators being used on the same function 
2. MeNeedsDecoration  = Decorator (MeNeedsDecoration)   /// 
   /// This returns the innerfunc object which is brilliant 
3. Decorators get executed in the order that they are defined - top to bottom 
4. MeNeedsMultipleDecorations =             (MeNeedsMultipleDecorations)

"""