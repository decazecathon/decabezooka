# decazooka006.py
#
# Building on top of decazooka001/002/003/004/005.py
# Please go through these programs first
#
#  The previous examples excluded a key complication
# The function and/or the decorators need arguments
#
""""
def-> def-> def

When writing decorators to handle arguments, the concept of closures come into the picture
>> The nested functions is able to access variables of the enclosing scope
>> The nested function remembers stuff (variable values) of the enclosing scope

This is the basic template when arguments are to be handled when using decorators
"""
def wrapper01 ( nlarg ):
    def inner_func ():
        return nlarg
    return inner_func

xfunc = wrapper01 ("200000000000000")
print ( xfunc())


def wrapper02 ( nlarg1, nlarg2 ):
    def inner_func (x,y):
        print ( x+y)
        return nlarg1 + nlarg2
    return inner_func

xfunc = wrapper02 (100, -100)
print ( xfunc(1,1))


def printex (*args, **kwargs):
    print ("*args and **kwargs in printex")

def recommender01 ( nlarg1, nlarg2 ):
    def first_wrapper (func):
        def second_wrapper (*args, **kwargs):
            print ( nlarg2 + nlarg1)
            func (*args, **kwargs)
        return second_wrapper
    return first_wrapper

print ("---------------------------------------------------------")
xfunc01 = recommender01 (100, -100)
xfunc02 = xfunc01 ( printex )
xfunc02 (*[1,2,3,4], **{"name": "sanjiv"})


def recommender02 ( func ):
    def first_wrapper (nlarg1, nlarg2):
        def second_wrapper (*args, **kwargs):
            print ( nlarg2 + nlarg1)
            func (*args, **kwargs)
        return second_wrapper
    return first_wrapper

print ("<<<<<<<<<<<---------------------------------------------------------")
xfunc01 = recommender02 (printex)
xfunc02 = xfunc01 ( 100, -100)
xfunc02 (*[1,2,3,4], **{"name": "sanjiv"})


print ("####--------------------------------------#### --------------------------")

#
#  Start from top i.e the outermost function and dive right in
#  This is how one is supposed to process nested functions
#  Remember the "inner" function remembers values from the enclosing scope
#
def recommender03 ( func, nlarg_01 ):
    def first_wrapper (nlarg_01_01):
        def second_wrapper (nlarg_01_01_01):
            def third_wrapper (*args, **kwargs):
                print ( nlarg_01 + nlarg_01_01 + nlarg_01_01_01)
                func (*args, **kwargs)
            return third_wrapper
        return second_wrapper
    return first_wrapper

xfunc01 = recommender03 (printex, 1000)
xfunc02 = xfunc01 ( 26)
xfunc03 = xfunc02 (24)
xfunc03 (*[1,2,3,4], **{"name": "sanjiv"})

