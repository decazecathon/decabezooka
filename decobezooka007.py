# decazooka007.py
#
# Building on top of decazooka001/002/003/004/005/006.py
# Please go through these programs first
#
#  The previous examples excluded a key complication
# The function and/or the decorators need arguments
#
""""
def-> def-> def

When writing decorators to handle arguments, the concept of closures come into the picture
>> The nested functions is able to access variables of the enclosing scope
>> The nested function remembers stuff (variable values) of the enclosing scope

This is the basic template when arguments are to be handled when using decorators
"""

import functools

def wrapper01 ( nlarg ):
    def inner_func ():
        return nlarg
    return inner_func

xfunc = wrapper01 ("200000000000000")
print ( xfunc())


def wrapper02 ( nlarg1, nlarg2 ):
    def inner_func (x,y):
        print ( x+y)
        return nlarg1 + nlarg2
    return inner_func

xfunc = wrapper02 (100, -100)
print ( xfunc(1,1))

def  add_spice_mix (ms1, ms2, ms3, ms4, ms5 ):
    #
    # The function to be decorated receives its arguments through the wrapper
    # This will preserve information about the original function.
    print ("add_spice_mix - just inside")


    def add_spice_mix_wrapper (myfunc):
        #
        print (add_spice_mix_wrapper.__closure__)
        print("add_spice_mix_wrapper - just inside ")
        @functools.wraps (myfunc)
        def func_wrapper(  *args, **kwargs):
            #

            print (func_wrapper.__closure__)
            print ("func_wrapper - just inside ")
            spiceList = [ms1, ms2, ms3, ms4, ms5]
            print (spiceList)
            myfunc(*args, **kwargs)
        print("func_wrapper - before the return ")
        return func_wrapper

    print("add_spice_mix_wrapper - before the return  ")
    return add_spice_mix_wrapper

@add_spice_mix("jeera", "salt", "pepper", "coriander", "hing")
def  meal ( *args, **kwargs ):
    print ("The meal is cooked")

meal()
