# decazooka003.py
#
# Building on top of decazooka001/002.py
# Please go through these two programs first
#

import functools
import inspect


def DecorativeHat001(targetfunc: object):
    #
    # This prevents a side effect of the decoration
    @functools.wraps(targetfunc)
    def innerfunc():
        #
        #print(":::1.1::: " + inspect.stack()[0][3] + " ::: PRE ")
        x = targetfunc()
        #print(":::1.1::: " + inspect.currentframe().f_code.co_name + " ::: POST")
        return x
    return innerfunc


@DecorativeHat001
def SimpleFunc001():
    return {"name": "sanjiv", "age": 51, "gender": "M"}

retval = SimpleFunc001()
print(retval)

@DecorativeHat001
def SimpleFunc002():
    return {"name": "ananya", "age": 17, "gender": "F"}


retval = SimpleFunc002()
print(retval)


"""
1. Same decorator being used across multiple functions 
2. MeNeedsDecoration  = Decorator (MeNeedsDecoration)   /// 
   /// This returns the innerfunc object which is brilliant 
"""